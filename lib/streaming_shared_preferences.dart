import 'package:flutter/services.dart';

typedef ValueObserver = void Function(String? value);

class StreamingSharedPreferences {
  static const MethodChannel _channel =
      const MethodChannel('streaming_shared_preferences');

  final Map<String, ValueObserver> _observers = {};
  final Map<String, String?> _lastValues = {};
  Duration _duration = Duration(milliseconds: 150);

  bool _isRunning = false;
  String? _nativePreferencesName;
  void setNativePreferencesName(String name) {
    _nativePreferencesName = name;
  }

  void setInterval(Duration duration) {
    if (_duration.inMilliseconds == 0) return;
    _duration = duration;
  }

  Future<String?> _getLatestValue(String key) {
    return _channel.invokeMethod<String>('getValue', {
      'key': key,
      'name': _nativePreferencesName,
    }).catchError((err) {
      return null;
    });
  }

  Future<bool?> setValue(String key, String value) {
    return _channel.invokeMethod<bool?>('setValue', {
      'key': key,
      'value': value,
      'name': _nativePreferencesName,
    }).catchError((err) => false);
  }

  void addObserver(String key, ValueObserver observer) {
    assert(!_isRunning);
    _observers.putIfAbsent(key, () => observer);
  }

  void run() {
    if (!_isRunning) {
      _isRunning = true;
      Future.doWhile(() async {
        await Future.forEach(
          _observers.keys,
          (dynamic element) async {
            try {
              String? value = await _getLatestValue(element);
              if (value == _lastValues[element]) return;
              _lastValues[element] = value;
              _observers[element]?.call(value);
            } catch (err) {
              print(err);
            }
          },
        );
        await Future.delayed(_duration);
        return _isRunning;
      });
    }
  }

  void removeObservers(String key) {
    if (_isRunning) return;
    _observers.remove(key);
    if (_observers.length == 0) {
      _isRunning = false;
    }
  }
}
