import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:streaming_shared_preferences/streaming_shared_preferences.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String value = 'default';
  @override
  void initState() {
    super.initState();
    StreamingSharedPreferences sp = StreamingSharedPreferences();
    sp.setNativePreferencesName('test');
    sp.addObserver('temp', (value) {
      print('update temp');
      setState(() {
        this.value = value;
      });
    });

    sp.run();

    Future.doWhile(() async {
      await Future.delayed(Duration(seconds: 1));
      sp.setValue('temp', Random().nextInt(100).toString());
      print('temp set');

      return true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('$value'),
        ),
      ),
    );
  }
}
